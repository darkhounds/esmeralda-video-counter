# Video Counter

## Run
- open this [link](https://darkhounds.gitlab.io/esmeralda-video-counter/)

## Coverting sec files into mp4
- install [FFmpeg](https://ffmpeg.zeranoe.com/builds/) locally. *( Version: 3.4.2, Architecture: Operating System version, Linking: static )*
- make the [FFmpeg](https://ffmpeg.zeranoe.com/builds/) executable available in the same directory as the .sec files. *( either by copying it directly into the same folder or adding it to the PATH env variable )*
- open a command line on the same folder as the .sec files. *( shift + right click the folder and choose open command line here )*
- replace *SOMEFILE* with the real file name on the the following commands, and execute them *( copy past it into the command line and press enter )*
    - ```./ffmpeg -i SOMEVIDEO.sec -c:v rawvideo -pix_fmt yuv420p buffer.yuv && ./ffmpeg -f rawvideo -pix_fmt yuv420p -s:v 704x576 -r 24 -c:v libx264 SOMEVIDEO.mp4```


